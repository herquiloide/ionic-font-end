import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RevendedoresPage } from './revendedores';

@NgModule({
  declarations: [
    RevendedoresPage,
  ],
  imports: [
    IonicPageModule.forChild(RevendedoresPage),
  ],
})
export class RevendedoresPageModule {}
