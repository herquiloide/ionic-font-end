import {RegistarMercadosPage} from "./registar-mercados/registar-mercados"
import {RegistarUnidadesMedidasPage} from "./registar-unidades-medidas/registar-unidades-medidas"

import {RegistarRevendedoresPage} from "./registar-revendedores/registar-revendedores"
import {RegistarProdutosPage} from "./registar-produtos/registar-produtos"
import {RegistarCategoriasPage} from "./registar-categorias/registar-categorias"
import {RegistarProdutoresPage} from "./registar-produtores/registar-produtores"

export {
  RegistarMercadosPage,
  RegistarProdutosPage,
  RegistarProdutoresPage,
  RegistarUnidadesMedidasPage,
  RegistarCategoriasPage,
  RegistarRevendedoresPage
}
