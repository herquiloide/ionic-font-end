import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistarUnidadesMedidasPage } from './registar-unidades-medidas';

@NgModule({
  declarations: [
    RegistarUnidadesMedidasPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistarUnidadesMedidasPage),
  ],
})
export class RegistarUnidadesMedidasPageModule {}
