import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistarRevendedoresPage } from './registar-revendedores';

@NgModule({
  declarations: [
    RegistarRevendedoresPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistarRevendedoresPage),
  ],
})
export class RegistarRevendedoresPageModule {}
