import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistarCategoriasPage } from './registar-categorias';

@NgModule({
  declarations: [
    RegistarCategoriasPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistarCategoriasPage),
  ],
})
export class RegistarCategoriasPageModule {}
