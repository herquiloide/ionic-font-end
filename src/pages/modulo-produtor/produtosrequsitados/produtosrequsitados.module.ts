import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProdutosrequsitadosPage } from './produtosrequsitados';

@NgModule({
  declarations: [
    ProdutosrequsitadosPage,
  ],
  imports: [
    IonicPageModule.forChild(ProdutosrequsitadosPage),
  ],
})
export class ProdutosrequsitadosPageModule {}
