import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisponibilizarProdutosPage } from './disponibilizar-produtos';

@NgModule({
  declarations: [
    DisponibilizarProdutosPage,
  ],
  imports: [
    IonicPageModule.forChild(DisponibilizarProdutosPage),
  ],
})
export class DisponibilizarProdutosPageModule {}
