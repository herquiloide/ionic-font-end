import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistarMeusProdutosPage } from './registar-meus-produtos';

@NgModule({
  declarations: [
    RegistarMeusProdutosPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistarMeusProdutosPage),
  ],
})
export class RegistarMeusProdutosPageModule {}
