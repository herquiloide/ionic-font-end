import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistarProdutosDisponibilizadosPage } from './registar-produtos-disponibilizados';

@NgModule({
  declarations: [
    RegistarProdutosDisponibilizadosPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistarProdutosDisponibilizadosPage),
  ],
})
export class RegistarProdutosDisponibilizadosPageModule {}
